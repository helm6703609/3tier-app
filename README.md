# 3tier-app

```
helm install -f .\db\values.yaml db ./db
helm install -f .\flask-backend\values.yaml backend ./flask-backend
helm install -f .\flask-frontend\values.yaml frontend ./flask-frontend

helm delete db
helm delete backend
helm delete frontend
```
